## Git-Based Modpack Manager
---

Script to update modpacks automatically

#### To Use

First, install [Python 3](https://www.python.org/downloads/) and [Git](https://git-scm.com/downloads) and add them to your `$PATH`.
Then, run `pip install requests` to install the Python Requests module (required to run the script).

Simply put the location of your `mods` folder in `pack-location.txt` and run `python update.py install`

#### Maintenance:

To check `version.txt` modlist for updates against `mods.txt` modlist, run `python update.py check_updates`.

To automatically populate `version.txt` with the most recent versions of mods listed in `mods.txt` run `python update.py apply_updates`.

Finally, to actually install mods from the list in `version.txt`, run `python update.py install`


***NOTE***: `check_updates` and `apply_updates` require you to have the `selenium` module installed
